#!/bin/bash

#--------------------------------------------------------------------------------------
# Gitlab-User Config:
export gitlabuser="change-me!"
export gitlabpwd="change-me!"
#--------------------------------------------------------------------------------------
version="0.0.0"


install(){

	clear
	
	echo "------------------------------------------------------"
	echo "Installing Prisma"
	echo "------------------------------------------------------"

	# check, if prisma is already installed, else break
	if [ -d ./prisma/ ]; then
		echo "Prisma seems already be installed. Please use command 'reinstall'"
		exit
	fi

	# check, if mongodb is already installed, else break
	if [ -d ./mongodb/ ]; then
		echo "Mongodb seems already be installed. Please use command 'reinstall'"
		exit
	fi

	# check, if pypy3 is already installed, else break
	if [ -d ./pypy3/ ]; then
		echo "Pypy3 seems already be installed. Please use command 'reinstall'"
		exit 
	fi

	# check, if gitlab user and password are set, else break

	if [ "$gitlabuser" == "change-me!" ] || [ "$gitlabpwd" == "change-me!" ]; then
		echo "Please set your Gitlab User and Password in this file and try again."
		exit
	fi

	# If folder is clean and gitlab user and password are set, we can continue with installation

	# Download and unpack Mongodb
	echo -n "Downloading Mongodb (100MB)..."
	wget "https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-ubuntu1604-3.4.6.tgz" -q -N -O mongodb.tgz
	mkdir mongodb && tar xzf mongodb.tgz -C mongodb --strip-components 1 >/dev/null
	mkdir -p ./mongodb/bin/data/db
	echo -e "DONE"

	# Download and unpack pypy3
	echo -n "Downloading Pypy3 (130MB)..."
	wget "https://gitlab.com/hashgraph/pypy3_prisma/raw/master/pypy3_prisma_amd64.tar" -q -N -O pypy3.tar
	mkdir pypy3 && tar xvf pypy3.tar -C pypy3 --strip-components 1 >/dev/null
	echo -e "DONE"

	# Download Prisma from Git
	echo -n "Downloading Prisma..."
	git clone https://"$gitlabuser":"$gitlabpwd"@gitlab.com/hashgraph/prisma -q
	echo -e "DONE"
	echo "------------------------------------------------------"
	echo "Installation Complete!"
	echo "Start prisma with './prisma-manager.sh start'"
}


update_prisma(){

	clear
	echo "------------------------------------------------------"
	echo "Updating Prisma"
	echo "------------------------------------------------------"
	
	rm -rf prisma/
	rm -rf .mongodb/bin/data/db/*
	git clone https://"$gitlabuser":"$gitlabpwd"@gitlab.com/hashgraph/prisma
}



start(){

	clear
	echo "------------------------------------------------------"
	echo "Starting Prisma"
	echo "------------------------------------------------------"
	echo -n "MongoDB..."	
	./mongodb/bin/mongod --dbpath ./mongodb/bin/data/db &>/dev/null &
	echo -e "DONE"
	echo -n "Prisma..."
	./pypy3/bin/pypy3.5 ./prisma/prismad.py --remoteport 8000 --remotenode 104.238.77.1 testing
	#echo -e "DONE"
}

stop(){

	clear
	echo "------------------------------------------------------"
	echo "Stopping Prisma"
	killall ./mongodb/bin/mongod &>/dev/null
	killall ./prisma/prismad.py &>/dev/null
	echo "------------------------------------------------------"
}


remove(){
	
		
	clear
	stop
	clear
	echo "------------------------------------------------------"
	echo "Removing Prisma"

	rm -rf prisma/
	rm -rf mongodb/
	rm -rf pypy3/
	echo "------------------------------------------------------"
}



help(){
	echo "------------------------------------------------------"
	echo "PRISMA MANAGER"
	echo "------------------------------------------------------"
	echo "Usage: ./prisma-manager.sh <command>"
	echo "Available Commands:
install - Gets mongodb, pypy3 and prisma without the need of installation or root.
update_prisma - Updates only prisma. All existing databases will be erased.
reinstall - Removes and installs mongodb, pypy3 and prisma again.
remove - Removes mongodb, pypy3 and prisma

start - Starts mongodb in background and prisma
stop - Stops mongodb and prisma	

help - Displays this helptext"

}


case "$1" in
	install)	install
		;;
	update_prisma)	stop
			update_prisma
		;;
	reinstall)	stop	
			remove
			install
		;;
	start)		stop
			start
		;;
	stop)		stop
		;;
	remove)		stop		
			remove
		;;
	help)		help
		;;
	*)		echo "NO INPUT! Use './prisma-manager.sh help' for more information"
		;;
esac
















